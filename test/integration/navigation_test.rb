require 'test_helper'

class NavigationTest < ActionDispatch::IntegrationTest

  setup { MongoMetrics::Metric.delete_all }

  test "does not log engine actions" do
    get mongo_metrics.root_path
    assert_equal 0, MongoMetrics::Metric.count
  end

  test "exports data to csv" do
    get main_app.home_foo_path
    get mongo_metrics.metrics_path(format: :csv)
    assert_match "process_action.action_controller,", response.body
  end

end

