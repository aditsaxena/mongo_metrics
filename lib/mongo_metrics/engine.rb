module MongoMetrics
  class Engine < ::Rails::Engine
    isolate_namespace MongoMetrics
    config.middleware.use MuteMiddleware
  end
end
