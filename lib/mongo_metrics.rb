require "active_support/notifications"

require "mongoid"
require "jquery-rails"

require "mongo_metrics/mute_middleware"
require "mongo_metrics/engine"
require "mongo_metrics/csv_streamer"

module MongoMetrics

  EVENT = "process_action.action_controller"
  ActiveSupport::Notifications.subscribe EVENT do |*args|
    MongoMetrics::Metric.store!(args) unless mute?
  end

  def self.mute!(&block)
    begin
      Thread.current["sql_metrics.mute"] = true
      yield
    rescue Exception => e
    ensure
      Thread.current["sql_metrics.mute"] = false
    end
  end

  def self.mute?
    Thread.current["sql_metrics.mute"] || false
  end

end
